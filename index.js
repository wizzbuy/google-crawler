var request = require('superagent');

var FRAGMENT = '?_escaped_fragment_=';

var RE_URLS = /(<a[^>]*href=\")(?:(?![a-zA-Z]+:))\/?([^"]*"[^>]*>)/gi;
var RE_SCRIPTS = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;

var DEFAULTS = {
  // Whether or not to build URLs with a shebang.
  shebang: true,

  // Default scraper URL; change this.
  scraper: 'http://localhost:8888/'
};

function urlfetch (options, url, callback) {
  // Communicate with PhantomJS through an HTTP request.
  request
    .get(options.scraper + url)
    .end(function (err, res) {
      if (err || !res.ok) {
        return callback(new Error('google-cralwer: not ok'));
      }

      // Obtain data from PhantomJS response. If at all.
      if (res.body) {
        if (res.body.data.length === 1) {
          if (res.body.data[0].data) {
            return callback(null, res.body.data[0].data);
          }
        }

        if (res.body.errors.length) {
          return callback(new Error('google-cralwer: errors on page'));
        }
      }

      callback(new Error('google-cralwer: unknown error'));
    });
}

module.exports = function (options) {
  if (!options) {
    // Provide default options if none are given.
    options = DEFAULTS;
  }
  else {
    // Provide a default `scraper` if not set.
    if (typeof options.scraper === 'undefined') {
      options.scraper = DEFAULTS.scraper;
    }

    // Provide a default `shebang` if not set.
    if (typeof options.shebang === 'undefined') {
      options.shebang = DEFAULTS.shebang;
    }
  }

  return function (req, res, next) {
    // Split incoming URL on the fragment.
    var parts = req.url.split(FRAGMENT);

    // If there's only one part, fragment was not found, bail.
    if (parts.length < 2) {
      return next();
    }

    // Build the base URL.
    var start = (options.protocol || req.protocol || 'http') + '://' +
      (options.host || req.headers.host);

    // Start building the URL.
    var url = start;

    // Append path to base URL.
    if (options.shebang) {
      // Using a shebang
      url += parts[0] + '#!' + decodeURIComponent(parts[1]);
    }
    else {
      // Not using a shebang
      url += parts[0] + decodeURIComponent(parts[1]);
    }

    // Remove duplicate slashes.
    url = url.replace(/\/+/g, '/');

    // Fetch HTML the server through a PhantomJS subsystem.
    urlfetch(options, url, function (err, data) {
      if (err) {
        return next(err);
      }

      // Ensure data is available.
      if (typeof data !== 'string' || !data) {
        return next(new Error('google-crawler: bad html'));
      }

      // Remove unwanted scripts and fix URLs.
      data = data
        .replace(RE_URLS, '$1' + start + '/$2')
        .replace(RE_SCRIPTS, '');

      // Send.
      res.end(data);
    });
  };
};
